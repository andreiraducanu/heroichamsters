﻿Heroic Hamsters
======

**Membri**

* Răducanu Andrei-Bogdan (SCRUM MASTER)
* Orășanu Diana
* Bulhac Vlad-Ionuț
* Burduhosu Mihai-Cristian
* Chontas Panagiotis-Efstratios
* Stavarache Antonio

**Aria de lucru** Backend

**Link de trello** https://trello.com/b/xyAkZvGa/heroic-hamsters

## Roadmap
- [x] Create C4 diagrams
- [x] Installing and configuring tools
- [x] Learn about MERN stack
- [x] Learn about Express + MongoDB
- [ ] Learn abot TypeScript
- [ ] Create basic functionality 
- [ ] Setting *models*
- [ ] Setting *routes*
- [ ] Connect with *frontend*
- [ ] Connect with *database*
- [ ] Connect with *machine learning*